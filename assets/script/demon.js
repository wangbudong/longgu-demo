// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    // update (dt) {},

    onCollisionEnter: function(other, self) {

        let animation = cc.find("hongdou").getComponent(dragonBones.ArmatureDisplay).armature().animation;
        if (animation.isPlaying && (animation.lastAnimationName == "Attack" || animation.lastAnimationName == "Attack_Down")) {
            cc.log("命中且正在播放攻击动画，进行伤害计算");
        }

    }
});