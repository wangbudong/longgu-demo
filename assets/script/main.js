// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        btnIdle: {
            type: cc.Node,
            default: null
        },
        btnAttack: {
            type: cc.Node,
            default: null
        },
        btnAttackDown: {
            type: cc.Node,
            default: null
        },
        btnDie: {
            type: cc.Node,
            default: null
        },
        btnStart: {
            type: cc.Node,
            default: null
        },
        hongdou: {
            type: cc.Node,
            default: null
        }

    },
    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        // Idle
        // Attack
        // Attack_Down
        // Die
        // Start
        cc.director.getCollisionManager().enabled = true;

        //this._armature.animation 
        // var NORMAL_ANIMATION_GROUP = "normal";
        // var AIM_ANIMATION_GROUP = "aim";
        // var ATTACK_ANIMATION_GROUP = "attack";
        this._armatureDisplay = this.hongdou.getComponent(dragonBones.ArmatureDisplay);
        this._armature = this._armatureDisplay.armature();
        cc.log(this._armature);

        //
        this.btnIdle.on(cc.Node.EventType.MOUSE_DOWN, this.funIdle, this);
        this.btnAttack.on(cc.Node.EventType.MOUSE_DOWN, this.funAttack, this);
        this.btnAttackDown.on(cc.Node.EventType.MOUSE_DOWN, this.funAttackDown, this);
        this.btnDie.on(cc.Node.EventType.MOUSE_DOWN, this.funDie, this);
        this.btnStart.on(cc.Node.EventType.MOUSE_DOWN, this.funStart, this);

    },
    onDestroy() {
        this.btnIdle.off(cc.Node.EventType.MOUSE_DOWN, this.funIdle, this);
        this.btnAttack.off(cc.Node.EventType.MOUSE_DOWN, this.funAttack, this);
        this.btnAttackDown.off(cc.Node.EventType.MOUSE_DOWN, this.funAttackDown, this);
        this.btnDie.off(cc.Node.EventType.MOUSE_DOWN, this.funDie, this);
        this.btnStart.off(cc.Node.EventType.MOUSE_DOWN, this.funStart, this);
    },
    start() {

    },
    funIdle(event) {
        this._armature.animation.fadeIn("Idle", -1, 0, 0, "default");
        cc.log(this._armature.animation);
    },
    funAttack(event) {
        this._armature.animation.fadeIn("Attack", -1, 0, 0, "default");
        cc.log(this._armature.animation);
    },
    funAttackDown(event) {
        this._armature.animation.fadeIn("Attack_Down", -1, 0, 0, "default");
        cc.log(this._armature.animation);
    },
    funDie(event) {
        this._armature.animation.fadeIn("Die", -1, 1, 0, "default");
        cc.log(this._armature.animation);
    },
    funStart(event) {
        this._armature.animation.fadeIn("Start", -1, 1, 0, "default");
        cc.log(this._armature.animation);
        let that = this;
        setTimeout(() => {
            that._armature.animation.fadeIn("Idle", -1, 0, 0, "default");
            cc.log(that._armature.animation);
        }, 1000);
    }


    // update (dt) {},
});